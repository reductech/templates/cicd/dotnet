include:
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

code_quality:
  stage: quality
  tags:
    - rt-dind
  cache: {}
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - when: manual
      allow_failure: true

security-code-scan-sast:
  stage: quality
  cache: {}
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /security-code-scan/
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_ID
    - when: manual
      allow_failure: true

secret_detection:
  stage: quality
  cache: {}
  rules:
    - if: $SECRET_DETECTION_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_ID
    - when: manual
      allow_failure: true

code coverage dev:
  stage: quality
  cache: {}
  extends:
    - .rules_dev_allowfailure
  needs:
    - test dev
    - version check dev
  variables:
    PROJECT_NAME: $CI_PROJECT_NAME
  before_script:
    - apt-get update && apt-get install --yes gpg
    - curl https://keybase.io/codecovsecurity/pgp_keys.asc | gpg --no-default-keyring --keyring trustedkeys.gpg --import
    - curl -Os https://uploader.codecov.io/latest/linux/codecov
    - curl -Os https://uploader.codecov.io/latest/linux/codecov.SHA256SUM
    - curl -Os https://uploader.codecov.io/latest/linux/codecov.SHA256SUM.sig
    - gpgv codecov.SHA256SUM.sig codecov.SHA256SUM
    - shasum -a 256 -c codecov.SHA256SUM
    - chmod +x codecov
    - dotnet tool install dotnet-reportgenerator-globaltool --tool-path ./dotnet-tools
  script:
    - >- # Normalize windows & linux paths
      pwsh -Command 'gci */coverage.cobertura.xml | % {
        [xml]$xml = Get-Content $_.FullName ;
        $source = $xml.coverage.sources.source ;
        $xml.coverage.sources.Item("source").IsEmpty = $true ;
        $xml.coverage.packages.package.classes.class | % {
          $_.filename = ($source + $_.filename) -replace "\\", "/" -replace ".+?${env:PROJECT_NAME}/"
        }
        $xml.Save($_.FullName)
      }'
    - ./dotnet-tools/reportgenerator "-reports:*/coverage.cobertura.xml" "-targetdir:." "-reporttypes:Cobertura"
    - ./codecov -f ./Cobertura.xml -t $CODECOV_TOKEN -b "$PROJECT_VERSION$VERSION_SUFFIX"
  coverage: /Coverage \(Branch\)[\s:]+(\d+\.?\d*)%/
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: Cobertura.xml

code coverage release:
  stage: quality
  extends:
    - code coverage dev
    - .rules_release_allowfailure
  needs:
    - test release
    - version check release

mutation testing:
  stage: quality
  extends:
    - .default_before_script
    - .rules_manual_allow_failure
  needs: []
  variables:
    PROJECT_TO_TEST: $CI_PROJECT_TITLE
    NUGET_PROJECT_ID: $NUGET_PROJECT_ID_DEV
    NUGET_USERNAME: $NUGET_USER_DEV
    NUGET_TOKEN: $NUGET_TOKEN_DEV
  script:
    - dotnet tool install dotnet-stryker --tool-path ./dotnet-tools
    - cd "$PROJECT_TO_TEST.Tests/"
    - ../dotnet-tools/dotnet-stryker --project "$PROJECT_TO_TEST.csproj"
      --reporter cleartext --reporter html --mutate "!['Log*','*Exception.ctor']"
    - cp ./StrykerOutput/*/reports/mutation-report.html ../
  artifacts:
    name: MutationReport
    expose_as: 'Mutation Report'
    paths:
      - mutation-report.html
    expire_in: never
